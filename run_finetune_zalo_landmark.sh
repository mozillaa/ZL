#!/bin/bash
set -e

# Where the training (fine-tuned) checkpoint and logs will be saved to.
TRAIN_DIR=./results

# Where the dataset is saved to.
DATASET_DIR=./zalo_landmark

if [ ! -d "$TRAIN_DIR" ]; then
  mkdir ${TRAIN_DIR}
fi

if [ ! -d "$DATASET_DIR" ]; then
  python download_zalo_landmark.py
fi
#Dropout=0 for inception, xception
network_list=("se_resnext50_32x4d" "inception_v4" "xception" "densenet161")
#dropout_list=("0.5" "0" "0" "0.5")
#image_size_list=("320" "320" "320" "288")
for network_name in "${network_list[@]}"; do
    # Fine-tune only the new layers
    python train_full.py \
        --train-dir=${TRAIN_DIR} \
        --dataset-dir=${DATASET_DIR} \
        --model-name=${network_name} \
        --image-size=320 \
        --imbalance=0 \
        --batch-size=32 \
        --dropout-p=0.5 \
        --test-batch-size=64 \
        --epochs=26
done